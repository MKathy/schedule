# Job Schedule

## Installation

1. Install xampp 7.3.11
2. Run Apache and MySQL
3. Clone repo to htdocs
4. Go to http://localhost/phpmyadmin/
5. Create new database 'Schedule'
6. Import sql/create_table.sql

#### Note
You can find credentials in config.php

